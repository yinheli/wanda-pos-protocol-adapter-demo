package cn.wanhui.pos.ext;

import cn.wanhui.pos.op.Transfer;
import cn.wanhui.pos.vo.FieldMappingConfig;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOSource;
import org.jpos.util.Log;
import org.jpos.util.LogSource;
import org.jpos.util.Logger;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author yinheli
 */
public class RequestListener implements ISORequestListener, Configurable, LogSource {

    private Log log;

    private Logger logger = null;

    private String realm = null;

    private FieldMappingConfig fieldMappingConfig;

    private ExecutorService handlerThreadPool = Executors.newCachedThreadPool(new NamedThreadFactory("request-handler-" + getRealm()));

    private static class NamedThreadFactory implements ThreadFactory {

        private AtomicLong threadNo = new AtomicLong(1);
        private final String  nameStart;
        private final String  nameEnd  = "]";

        private NamedThreadFactory(String poolName) {
            this.nameStart = "[" + poolName + "-";
        }

        @Override
        public Thread newThread(Runnable r) {
            Thread newThread = new Thread(r, nameStart + threadNo.getAndIncrement() + nameEnd);
            newThread.setDaemon(false);
            if (newThread.getPriority() != Thread.NORM_PRIORITY) {
                newThread.setPriority(Thread.NORM_PRIORITY);
            }
            return newThread;
        }
    }

    /**
     * @param source source where you optionally can reply
     * @param m      the unmatched request
     * @return true if request was handled by this listener
     */
    @Override
    public boolean process(final ISOSource source, final ISOMsg m) {
        handlerThreadPool.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    new Transfer().doTrx(source, m, RequestListener.this.fieldMappingConfig);
                } catch (Exception e) {
                    RequestListener.this.log.error("trans process exception", e);
                }
            }
        });

        return true;
    }

    /**
     * @param cfg Configuration object
     * @throws org.jpos.core.ConfigurationException
     */
    @Override
    public void setConfiguration(Configuration cfg) throws ConfigurationException {
        log = new Log(getLogger(), getRealm());

        String mappingFile = cfg.get("field-mapping");
        if (mappingFile != null) {
            try {
                fieldMappingConfig = parse(mappingFile);
            } catch (Exception e) {
                throw new ConfigurationException("parse mappingFile exception", e);
            }
        }
    }

    private FieldMappingConfig parse(String mappingFile) throws Exception {
        FieldMappingConfig config = new FieldMappingConfig();
        SAXBuilder builder = new SAXBuilder();
        Document document = builder.build(new File(mappingFile));
        Element rootNode = document.getRootElement();
        List list = rootNode.getChildren("field");
        for (int i = 0; i < list.size(); i++) {
            Element node = (Element) list.get(i);
            config.set(node.getAttribute("id").getIntValue(), node.getAttributeValue("name"));
        }
        return config;
    }

    @Override
    public void setLogger(Logger logger, String realm) {
        this.logger = logger;
        this.realm = realm;
    }

    @Override
    public String getRealm() {
        return realm;
    }

    @Override
    public Logger getLogger() {
        return logger;
    }
}


