package cn.wanhui.pos.vo;

import java.util.HashMap;

/**
 * @author yinheli
 */
public class FieldMappingConfig {

    private HashMap<Integer, String> idToName = new HashMap<Integer, String>();
    private HashMap<String, Integer> nameToId = new HashMap<String, Integer>();

    public FieldMappingConfig() {
        for (int i = 0; i <= 64; i++) {
            set(i, "f"+i);
        }
    }

    public String getMappingName(int id) {
        return idToName.get(id);
    }

    public int getMappingId(String name) {
        return nameToId.get(name);
    }

    public void set(int id, String name) {
        idToName.put(id, name);
        nameToId.put(name, id);
    }

}
