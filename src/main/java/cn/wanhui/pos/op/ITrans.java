package cn.wanhui.pos.op;

import cn.wanhui.pos.vo.FieldMappingConfig;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOSource;

/**
 * @author yinheli
 */
public interface ITrans {

    public void doTrx(ISOSource source, ISOMsg m, FieldMappingConfig mappingConfig) throws Exception;

}
