package cn.wanhui.pos.op;

import cn.wanhui.pos.vo.FieldMappingConfig;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOSource;

/**
 * @author yinheli
 */
public class Transfer implements ITrans {

    @Override
    public void doTrx(ISOSource source, ISOMsg m, FieldMappingConfig mappingConfig) throws Exception {
        // build http request
        // print http request params
        for (int i = 0, j = m.getMaxField(); i <= j; i++) {
            if (m.hasField(i)) {
                String str = mappingConfig.getMappingName(i) + ":" + m.getComponent(i).getValue();
                System.out.println(str);
            }
        }

        // http request
        // TODO

        // parse result, send response to pos
        // TODO
    }

}
